package jp.boo.mekefactory.sampleffmpeg.util.ffmpeg;

/**
 * Created by mekefactory on 2017/12/11.
 */

public interface FFmpegWrapListener {
    void onProgress(float progress);
    void onFinish(boolean isSuccess, String message);
    void onDuration(boolean isSuccess, long duration);
}
