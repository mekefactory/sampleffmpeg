package jp.boo.mekefactory.sampleffmpeg.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

/**
 * Created by mekefactory on 2017/04/22.
 */

public class Helper {
    public static final int REQUEST_PERMISSION = 1000;

    /**
     * permissionの確認
     */
    public static void checkPermission(Activity activity) {
        if (ActivityCompat.checkSelfPermission(
                activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION);
        }
    }
}
