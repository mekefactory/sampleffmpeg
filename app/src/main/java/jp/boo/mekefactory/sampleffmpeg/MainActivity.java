package jp.boo.mekefactory.sampleffmpeg;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import jp.boo.mekefactory.sampleffmpeg.util.Helper;
import jp.boo.mekefactory.sampleffmpeg.util.ffmpeg.FFmpegWrap;
import jp.boo.mekefactory.sampleffmpeg.util.ffmpeg.FFmpegWrapAdapter;

import static android.os.Environment.DIRECTORY_MOVIES;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {
    private static String TAG = MainActivity.class.getSimpleName();

    private FFmpegWrap ffmpegWrap;
    private File editingFile;
    private long editingFileDuration = -1;
    private File outputFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= 23) {
            Helper.checkPermission(this);
        }

        try {
            ffmpegWrap = FFmpegWrap.getInstance(this);
        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
        }
    }

    @ViewById(R.id.myTextView)
    TextView myTextView;

    @Click(R.id.selectButton)
    void selectVideo() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("video/*");
        startActivityForResult(intent, 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (resultData == null) {
            return;
        }
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case 100: {
                // ギャラリーからの時
                String strDocId = DocumentsContract.getDocumentId(resultData.getData());
                String[] strSplittedDocId = strDocId.split(":");
                String strId = strSplittedDocId[strSplittedDocId.length - 1];
                Cursor crsCursor = getContentResolver().query(
                        MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                        new String[]{MediaStore.MediaColumns.DATA},
                        "_id=?",
                        new String[]{strId},
                        null
                );
                crsCursor.moveToFirst();
                File inputFile = new File(crsCursor.getString(0));

                String id = UUID.randomUUID().toString();
                editingFile = new File(
                        new StringBuilder()
                                .append(getExternalFilesDir(DIRECTORY_MOVIES).getPath())
                                .append("/")
                                .append(id)
                                .append("_editing.mp4")
                                .toString());
                outputFile = new File(
                        new StringBuilder()
                                .append(getExternalFilesDir(DIRECTORY_MOVIES).getPath())
                                .append("/")
                                .append(id)
                                .append("_edited.mp4")
                                .toString());
                try {
                    FileUtils.copyFile(inputFile, editingFile);
                    Log.i(TAG, "in:" + inputFile.getAbsolutePath()
                            + " out:" + editingFile.getAbsolutePath());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                ffmpegWrap.duration(editingFile,
                        new FFmpegWrapAdapter() {
                            @Override
                            public void onDuration(boolean isSuccess, long duration) {
                                Log.i(TAG, "onDuration. isSuccess:" + isSuccess + " duration:" + duration);
                                if(isSuccess) {
                                }
                                myTextView.setText(String.valueOf(duration));
                                editingFileDuration = duration;
                            }
                        });
            }
        }
    }

    @Click(R.id.durationButton)
    void durationButton() {
        myTextView.setText("duration...");

        ffmpegWrap.duration(editingFile,
                new FFmpegWrapAdapter() {
                    @Override
                    public void onDuration(boolean isSuccess, long duration) {
                        Log.i(TAG, "onDuration. isSuccess:" + isSuccess + " duration:" + duration);
                        if(isSuccess) {
                        }
                        myTextView.setText(String.valueOf(duration));
                        editingFileDuration = duration;
                    }
                });
    }

    @Click(R.id.cutButton)
    void cutButton() {
        myTextView.setText("cut...");

        try {
            FileUtils.forceDelete(outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ffmpegWrap.cut(editingFile, outputFile, 1000, 15000,
                new FFmpegWrapAdapter() {
                    @Override
                    public void onProgress(float progress) {
                        Log.i(TAG, "onProgress. progress:" + progress);
                        myTextView.setText((int)(progress*100) + "%");
                    }

                    @Override
                    public void onFinish(boolean isSuccess, String message) {
                        Log.i(TAG, "onFinish. isSuccess:" + isSuccess + " message:" + message);
                        if(isSuccess) {
                            ContentValues values = new ContentValues();
                            ContentResolver contentResolver = getContentResolver();
                            values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
                            values.put(MediaStore.Video.Media.TITLE, outputFile.getName());
                            values.put("_data", outputFile.getAbsolutePath());
                            contentResolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
                        }
                        myTextView.setText(message);
                    }
                });
    }

    @Click(R.id.joinButton)
    void joinButton() {
        myTextView.setText("join...");

        try {
            FileUtils.forceDelete(outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<File> inFiles = Arrays.asList(editingFile, editingFile, editingFile);
        ffmpegWrap.join(inFiles, outputFile,
                editingFileDuration*3,
                new FFmpegWrapAdapter() {
                    @Override
                    public void onProgress(float progress) {
                        Log.i(TAG, "onProgress. progress:" + progress);
                        myTextView.setText((int)(progress*100) + "%");
                    }

                    @Override
                    public void onFinish(boolean isSuccess, String message) {
                        Log.i(TAG, "onFinish. isSuccess:" + isSuccess + " message:" + message);
                        if(isSuccess) {
                            ContentValues values = new ContentValues();
                            ContentResolver contentResolver = getContentResolver();
                            values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
                            values.put(MediaStore.Video.Media.TITLE, outputFile.getName());
                            values.put("_data", outputFile.getAbsolutePath());
                            contentResolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
                        }
                        myTextView.setText(message);
                    }
                });
    }
}
