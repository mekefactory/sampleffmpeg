package jp.boo.mekefactory.sampleffmpeg.util.ffmpeg;

import android.content.Context;
import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import org.apache.commons.lang3.time.DurationFormatUtils;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mekefactory on 2017/12/09.
 */

public class FFmpegWrap {
    private static String TAG = FFmpegWrap.class.getSimpleName();

    private static FFmpegWrap instance = new FFmpegWrap();
    private static FFmpeg ffmpeg;
    private static Context context;

    private FFmpegWrap() {
    }

    public static FFmpegWrap getInstance(Context cx) throws FFmpegNotSupportedException {
        if(ffmpeg == null) {
            context = cx;
            ffmpeg = FFmpeg.getInstance(context);
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    Log.i(TAG, "loadBinary.onFailure");
                }

                @Override
                public void onSuccess() {
                    Log.i(TAG, "loadBinary.onSuccess");
                }

                @Override
                public void onFinish() {
                    Log.i(TAG, "loadBinary.onFinish");
                }
            });
        }
        return instance;
    }

    /**
     * 動画長の取得
     * ffmpeg -i input.mp4
     * @param inFile
     * @param adapter
     */
    public void duration(File inFile,
                         final FFmpegWrapAdapter adapter) {
        try {
            List<String> cmdList = new ArrayList<String>();
            cmdList.add("-i");
            cmdList.add(inFile.getAbsolutePath());

            String[] cmd = cmdList.toArray(new String[0]);
            ffmpeg.execute(cmd, new ExecuteBinaryResponseHandler() {
                private long duration = -1;

                @Override
                public void onProgress(String message) {
                    Log.i(TAG, "execute.onProgress. message:" + message);
                    if(!message.contains("Duration:")) {
                        return;
                    }

                    // "  Duration: 00:00:06.04, start: 0.000000, bitrate: 13086 kb/s
                    int beginIndex = message.indexOf("Duration: ") + 10;
                    int endIndex = message.indexOf(", start:");
                    message = message.substring(beginIndex, endIndex);

                    try {
                        duration = convertTime(message);
                        Log.i(TAG, "contain Duration. message:[" + message
                                + "] duration:" + duration);
                    } catch (ParseException e) {
                        Log.e(TAG, "error", e);
                    }
                }

                @Override
                public void onFailure(String message) {
                    Log.i(TAG, "execute.onFailure. message:" + message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.i(TAG, "execute.onSuccess. message:" + message);
                }

                @Override
                public void onFinish() {
                    Log.i(TAG, "execute.onFinish. duration:" + duration);
                    adapter.onDuration(true, duration);
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            Log.e(TAG, "error", e);
            adapter.onFinish(false, e.getMessage());
        }
    }

    /**
     * cut
     * ffmpeg -i input.mp4 -t 15 -ss 45 out.mp4
     * @param inFile
     * @param outFile
     * @param start
     * @param duration
     * @param adapter
     */
    public void cut(File inFile, final File outFile,
                    long start, final long duration,
                    final FFmpegWrapAdapter adapter) {
        try {
            if(ffmpeg.isFFmpegCommandRunning()) {
                ffmpeg.killRunningProcesses();
            }

            List<String> cmdList = new ArrayList<String>();
            cmdList.add("-i");
            cmdList.add(inFile.getAbsolutePath());
            cmdList.add("-t");
            cmdList.add(DurationFormatUtils.formatPeriod(0, duration, "HH:mm:ss.SSS"));
            cmdList.add("-ss");
            cmdList.add(DurationFormatUtils.formatPeriod(0, start, "HH:mm:ss.SSS"));
            cmdList.add(outFile.getAbsolutePath());

            String[] cmd = cmdList.toArray(new String[0]);
            ffmpeg.execute(cmd, new ExecuteBinaryResponseHandler() {
                private boolean isSuccess = false;

                @Override
                public void onProgress(String message) {
                    Log.i(TAG, "execute.onProgress. message:" + message);

                    // frame= 1584 fps= 27 q=-1.0 Lsize=    2248kB time=00:01:06.01 bitrate= 278.9kbits/s speed=1.14x
                    if(!message.contains(" time=")) {
                        return;
                    }

                    int beginIndex = message.indexOf(" time=") + 6;
                    int endIndex = message.indexOf(" bitrate=");
                    message = message.substring(beginIndex, endIndex);

                    try {
                        long time = convertTime(message);
                        float ratio = (float)time / (float)duration;
                        adapter.onProgress(ratio);
                    } catch (ParseException e) {
                        Log.e(TAG, "error", e);
                    }
                }

                @Override
                public void onFailure(String message) {
                    Log.i(TAG, "execute.onFailure. message:" + message);
                    isSuccess = false;
                }

                @Override
                public void onSuccess(String message) {
                    Log.i(TAG, "execute.onSuccess. message:" + message);
                    isSuccess = true;
                }

                @Override
                public void onFinish() {
                    Log.i(TAG, "execute.onFinish. isSuccess:" + isSuccess);
                    adapter.onFinish(isSuccess, Boolean.toString(isSuccess));
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            Log.e(TAG, "error", e);
            adapter.onFinish(false, e.getMessage());
        }
    }

    /**
     * join
     * ffmpeg -i [infile1] -i [infile2] -filter_complex "concat=n=2:v=1:a=1" [outfile]
     * -i　で入力ファイルを指定します。
     * n は連結するファイル数
     * v は連結する映像ファイルの可否 1がtrue, 0がfalse
     * a は連結する音声ファイルの可否 1がtrue, 0がfalse
     * @param inFiles
     * @param outFile
     * @param duration
     * @param adapter
     */
    public void join(List<File> inFiles, final File outFile,
                     final long duration,
                     final FFmpegWrapAdapter adapter) {
        try {
            List<String> cmdList = new ArrayList<String>();
            for(File inFile : inFiles) {
                cmdList.add("-i");
                cmdList.add(inFile.getAbsolutePath());
            }
            cmdList.add("-filter_complex");
            cmdList.add("concat=n=" + inFiles.size() + ":v=1:a=1");
            cmdList.add(outFile.getAbsolutePath());

            String[] cmd = cmdList.toArray(new String[0]);
            ffmpeg.execute(cmd, new ExecuteBinaryResponseHandler() {
                private boolean isSuccess = false;

                @Override
                public void onProgress(String message) {
                    Log.i(TAG, "execute.onProgress. message:" + message);

                    // frame=   66 fps=7.9 q=29.0 size=      99kB time=00:00:02.00 bitrate= 402.4kbits/s dup=1 drop=0 speed=0.241x
                    if(!message.contains(" time=")) {
                        return;
                    }

                    int beginIndex = message.indexOf(" time=") + 6;
                    int endIndex = message.indexOf(" bitrate=");
                    message = message.substring(beginIndex, endIndex);

                    try {
                        long time = convertTime(message);
                        float ratio = (float)time / (float)duration;
                        adapter.onProgress(ratio);
                    } catch (ParseException e) {
                        Log.e(TAG, "error", e);
                    }
                }

                @Override
                public void onFailure(String message) {
                    Log.i(TAG, "execute.onFailure. message:" + message);
                    isSuccess = false;
                }

                @Override
                public void onSuccess(String message) {
                    Log.i(TAG, "execute.onSuccess. message:" + message);
                    isSuccess = true;
                }

                @Override
                public void onFinish() {
                    Log.i(TAG, "execute.onFinish. isSuccess:" + isSuccess);
                    adapter.onFinish(isSuccess, Boolean.toString(isSuccess));
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            Log.e(TAG, "error", e);
            adapter.onFinish(false, e.getMessage());
        }
    }

    public static long convertTime(String HHmmssSSS) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        long time1 = format.parse("1970/01/01 " + HHmmssSSS).getTime();
        long time2 = format.parse("1970/01/01 00:00:00.00").getTime();
        return time1 - time2;
    }
}
