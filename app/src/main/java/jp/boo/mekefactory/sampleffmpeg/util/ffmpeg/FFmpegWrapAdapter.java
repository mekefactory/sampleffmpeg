package jp.boo.mekefactory.sampleffmpeg.util.ffmpeg;

/**
 * Created by mekefactory on 2017/12/11.
 */

public class FFmpegWrapAdapter implements FFmpegWrapListener {
    @Override
    public void onProgress(float progress) {
    }

    @Override
    public void onFinish(boolean isSuccess, String message) {
    }

    @Override
    public void onDuration(boolean isSuccess, long duration) {
    }
}
